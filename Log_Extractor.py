import linecache
import logging
import datetime
import math
import time as tm
import sys

def reverse(string): 
    string = string[::-1] 
    return string 

def Generate_LogFile(Dir):
	filename = "file.txt"
	count = 0

	for j in range(1, 10):

		count = count + 1
		val = count
		digits = int(math.log10(count))+1
		zero = 6 - digits
		val = str(val)
		val = val.ljust(zero + len(val), "0") 
		val = reverse(val)

		file = open(Dir+"/LogFile-" + val +".log", "w")

		print("Log_File_Directory/file" + str(j) +".txt Start")

		for i in range(0, 90000000):
			time = str(datetime.datetime.now().date()) + "T"+str(datetime.datetime.now().time())+","
			file.write(time)
			file.write("\n")


def Create_SecondaryFile(Log_Dir, No_Of_LogFile):

	Secondary_File = open("Secondary_File.txt","w")

	count = 0
	for i in range(1, No_Of_LogFile+1):

		count = count + 1
		val = count
		digits = int(math.log10(count))+1
		zero = 6 - digits
		val = str(val)
		val = val.ljust(zero + len(val), "0") 
		val = reverse(val)
		filename = Log_Dir+"/LogFile-" + val +".log"

		with open(filename, 'r') as f:
			lines = f.readline().split("\n")[0] + "~" + filename
			Secondary_File.write(lines)
			Secondary_File.write("\n")



def Searching_In_Secondary(initial_time, final_time):
	Secondary_File = open("Secondary_File.txt","r")
	Prev_File = ""
	file = Secondary_File.read().split("\n")
	
	file.pop()
	for val in file:
		First_FileName = Prev_File
		Prev_File = val.split("~")[1]

		if initial_time <= val.split("~")[0]:
			break

	Prev_File = ""
	for val in file:
		Second_FileName = Prev_File
		Prev_File = val.split("~")[1]
		
		if final_time <= val.split("~")[0]:
			break


	return First_FileName, Second_FileName



file_ptr = ""
def read_in_chunks(file_object, chunk_size=1024):
	global file_ptr

	while True:
		data = file_object.read(chunk_size)
		file_ptr = file_object

		if not data:
			break
		yield data


def Print_Logs(file_ptr, final_time, flag, First_FileName, Second_FileName):

	if flag == 2:
		line = file_ptr.readline()
		list_line = line.split(",")[0]

		while list_line <= final_time :
			# print(line)
			Final_Log_Data.append(line)
			line = file_ptr.readline()
			list_line = line.split(",")[0]

		# print(line)
		Final_Log_Data.append(line)


	signal = 0
	if flag == 1:

		for k in file_names:
		
			if k == First_FileName:
				signal = 1
				continue

			if signal == 1:
				file_next = open(k, "r")
				
				line = file_next.readline()
				list_line = line.split(",")[0]

				while list_line <= final_time:
					
					Final_Log_Data.append(line)
					line = file_next.readline()
					list_line = line.split(",")[0]
					# print(line)
					

					
			if k == Second_FileName:
				signal = 0
				file_next = open(k, "r")

				line = file_next.readline()
				list_line = line.split(",")[0]

				while list_line <= final_time:
					# print(line)
					Final_Log_Data.append(line)
					line = file_next.readline()
					list_line = line.split(",")[0]

				# print(line)
				Final_Log_Data.append(line)

				break





def Printing_Logs(First_FileName, Second_FileName, initial_time, final_time):

	global file_ptr

	f = open(First_FileName, 'r') 

	for piece in read_in_chunks(f, 536870912):

		piece = piece +f.readline()
		List_Of_Log = piece.split("\n")
		List_Of_Log.pop()
		last_element = List_Of_Log[-1].split(",")[0]


		if initial_time <= last_element:

			for i in range(0, len(List_Of_Log)):
				if initial_time >= List_Of_Log[i]:
					continue

				# print(List_Of_Log[i])
				Final_Log_Data.append(List_Of_Log[i])

			break

	if First_FileName == Second_FileName:
		Print_Logs(file_ptr, final_time, 2, First_FileName, Second_FileName)

	else:
		Print_Logs(file_ptr, final_time, 1, First_FileName, Second_FileName)
			


def Write_Logs_File():
	file = open("Output/Output.txt", "w")
	for line in Final_Log_Data:
		print(line)
		file.write(line)
		file.write("\n")




# Generate_LogFile("Log_File_Directory")

Final_Log_Data = []

n = len(sys.argv) 

# initial_time =   sys.argv[1]
# final_time   =   sys.argv[2]

initial_time =   "2020-06-21T08:42:40.708442,"
final_time   =   "2020-06-21T08:42:56.727197,"

Log_Dir = "Log_File_Directory"
# sys.argv[3]

No_Of_LogFile = int(input("Enter No. of log files : "))

file_names = []

c = 0
for i in range(1, 1000):

	c = c + 1
	val = count
	digits = int(math.log10(c))+1
	zero = 6 - digits
	val = str(val)
	val = val.ljust(zero + len(val), "0") 
	val = reverse(val)
	file_names.append("Log_File_Directory/LogFile-" + val +".log")


Create_SecondaryFile(Log_Dir, No_Of_LogFile)
First_FileName, Second_FileName = Searching_In_Secondary(initial_time, final_time)
print(First_FileName, Second_FileName)
Printing_Logs(First_FileName, Second_FileName,initial_time, final_time)
Write_Logs_File()